"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
class Tracker {
    /**
     * Constructs a new instance of the API class.
     * @param {string} url - The URL for the API.
     * @param {string} [previewToken] - The preview token (optional).
     */
    constructor(url, trackerType, previewToken) {
        this.previewToken = null;
        this.user = null;
        this.device = null;
        this.isInitialized = false;
        this.url = url;
        this.trackerType = trackerType;
        previewToken && (this.previewToken = previewToken);
    }
    /**
     * Sets the user information.
     *
     * @param {UserInput} user - The user information to be set.
     * @returns {void} - Returns nothing.
     */
    setUserProperties(user) {
        const { id, code, point } = user;
        this.user = {
            user_id: id,
            customer_id: id,
            user_code: code,
            point_balance: point,
        };
    }
    /**
     * Sets the point balance for the user.
     *
     *  @param balance - The new point balance for the user.
     */
    setPointBalance(balance) {
        this.user.point_balance = balance;
    }
    /**
     * Set the device information.
     *
     * @param {DeviceInfo} device - The device information to set.
     * @returns {void}
     */
    setDeviceInfo(device) {
        this.device = Object.assign({}, device);
    }
    /**
     * Initializes the user and device information.
     *
     * @param {DeviceInfo} device - The device information object.
     * @return {void} No return value.
     */
    initialize(device) {
        this.setDeviceInfo(device);
        this.isInitialized = true;
    }
    /**
     * Destroys the object by setting the `user` and `device` properties to null and the `isInitialized` property to false.
     *
     * @param {void} - This function does not accept any parameters.
     * @return {void} - This function does not return any value.
     */
    destroy() {
        this.user = null;
        this.device = null;
        this.isInitialized = false;
    }
    transform(event) {
        if (!this.isInitialized)
            return;
        const { name, properties, screenName } = event;
        const { utm_source, platform, app } = this.device;
        const today = new Date();
        const commonProperties = {
            hour_of_day: today.getHours(),
            day_of_week: constants_1.COMMON.DAY_OF_WEEK[today.getDay()],
            day_of_month: today.getDate(),
            utm_source,
            platform,
            page_title: screenName,
            front_end: app,
        };
        const trackingProperties = Object.assign(Object.assign({}, properties), commonProperties);
        const data = {
            user_properties: this.user ? this.user : undefined,
            events: [{ name, params: trackingProperties }],
        };
        return data;
    }
    track(event) {
        if (!this.isInitialized)
            return;
        const data = this.transform(event);
        const headers = {
            'Content-Type': 'application/json',
            'X-Gtm-Server-Preview': this.previewToken,
        };
        if (!this.previewToken) {
            delete headers['X-Gtm-Server-Preview'];
        }
        fetch(this.url, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            headers,
            body: JSON.stringify(data),
        });
    }
}
exports.default = Tracker;
//# sourceMappingURL=tracker.js.map