"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FrontendSource = exports.Platform = void 0;
var Platform;
(function (Platform) {
    Platform["ANDROID"] = "ANDROID";
    Platform["IOS"] = "iOS";
    Platform["WEB"] = "web";
    Platform["UNKNOWN"] = "unknown";
})(Platform || (exports.Platform = Platform = {}));
var FrontendSource;
(function (FrontendSource) {
    FrontendSource["APP"] = "Native App";
    FrontendSource["MOMO"] = "MoMo Mini App";
    FrontendSource["WEB"] = "Web Browser";
})(FrontendSource || (exports.FrontendSource = FrontendSource = {}));
//# sourceMappingURL=enum.js.map