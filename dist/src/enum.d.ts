export declare enum Platform {
    ANDROID = "ANDROID",
    IOS = "iOS",
    WEB = "web",
    UNKNOWN = "unknown"
}
export declare enum FrontendSource {
    APP = "Native App",
    MOMO = "MoMo Mini App",
    WEB = "Web Browser"
}
