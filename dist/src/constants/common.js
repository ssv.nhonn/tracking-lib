"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.COMMON = void 0;
const DAY_OF_WEEK = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const TRACKING_FRONTEND = {
    APP: 'Native App',
    WEB: 'Web Browser',
    MINIAPP: 'MoMo Mini App',
};
exports.COMMON = {
    DAY_OF_WEEK,
    TRACKING_FRONTEND,
};
//# sourceMappingURL=common.js.map