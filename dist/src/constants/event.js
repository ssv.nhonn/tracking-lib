"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EVENT = void 0;
exports.EVENT = {
    ALL_CLICKS: 'all_clicks',
    ERROR_JS: 'error_js',
    SCREEN_VIEW: 'screen_view',
    CAMPAIGN_CLICK: 'campaign_click',
    CAMPAIGN_DETAIL_VIEW: 'campaign_detail_view',
    CLICK_SUPPORT: 'click_support',
    FEEDBACK_START: 'feedback_start',
    ADD_TO_CART: 'add_to_cart',
    ADDRESS_ENTERED: 'address_entered',
    BANNER_CLICK: 'banner_click',
    BEGIN_CHECKOUT: 'begin_check_out',
    GIFT_CODE_ENTERED: 'gift_code_entered',
    GIFT_CODE_ENTERED_FAILED: 'gift_code_entered_failed',
    GIFT_CODE_ENTERED_SUCCESS: 'gift_code_entered_success',
    GROUP_CLICK: 'group_click',
    LISTING_DETAIL_VIEW: 'listing_detail_view',
    LISTING_MODIFY: 'listing_modify',
    ORDER_CANCEL_CLICK: 'order_cancel_click',
    OUT_OF_STOCK: 'out_of_stock',
    PURCHASE: 'purchase',
    REMOVE_FROM_CART: 'remove_from_cart',
    SCREENSHOT: 'screenshot',
    SEARCH: 'search',
    SIGN_IN: 'sign_in',
    SIGN_IN_START_STEP_1: 'sign_in_start_step_1',
    SIGN_IN_ENTER_CREDENTIAL_STEP_2: 'sign_in_enter_credential_step_2',
    SIGN_IN_ENTER_OTP_STEP_3: 'sign_in_enter_otp_step_3',
    SEARCH_HISTORY_CLICK: 'search_history_click',
    SEARCH_POPULAR_CLICK: 'search_popular_click',
    SHARE_CLICK: 'share_click',
    SYSTEM_SESSION_START: 'system_session_start',
    VOUCHER_DETAIL_VIEW: 'voucher_detail_view',
};
//# sourceMappingURL=event.js.map