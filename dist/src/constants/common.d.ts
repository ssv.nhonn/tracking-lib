export declare const COMMON: {
    DAY_OF_WEEK: string[];
    TRACKING_FRONTEND: {
        APP: string;
        WEB: string;
        MINIAPP: string;
    };
};
