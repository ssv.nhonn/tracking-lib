import { DeviceInfo, TrackerType, TrackingData, TrackingInputInfo, UserInput } from './interface';
export default class Tracker {
    private url;
    private previewToken;
    private user;
    private device;
    private isInitialized;
    private trackerType;
    /**
     * Constructs a new instance of the API class.
     * @param {string} url - The URL for the API.
     * @param {string} [previewToken] - The preview token (optional).
     */
    constructor(url: string, trackerType: TrackerType, previewToken?: string);
    /**
     * Sets the user information.
     *
     * @param {UserInput} user - The user information to be set.
     * @returns {void} - Returns nothing.
     */
    setUserProperties(user: UserInput): void;
    /**
     * Sets the point balance for the user.
     *
     *  @param balance - The new point balance for the user.
     */
    setPointBalance(balance: number): void;
    /**
     * Set the device information.
     *
     * @param {DeviceInfo} device - The device information to set.
     * @returns {void}
     */
    setDeviceInfo(device: DeviceInfo): void;
    /**
     * Initializes the user and device information.
     *
     * @param {DeviceInfo} device - The device information object.
     * @return {void} No return value.
     */
    initialize(device: DeviceInfo): void;
    /**
     * Destroys the object by setting the `user` and `device` properties to null and the `isInitialized` property to false.
     *
     * @param {void} - This function does not accept any parameters.
     * @return {void} - This function does not return any value.
     */
    destroy(): void;
    transform(event: TrackingInputInfo): TrackingData;
    track(event: TrackingInputInfo): void;
}
