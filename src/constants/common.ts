const DAY_OF_WEEK = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const TRACKING_FRONTEND = {
  APP: 'Native App',
  WEB: 'Web Browser',
  MINIAPP: 'MoMo Mini App',
};

export const COMMON = {
  DAY_OF_WEEK,
  TRACKING_FRONTEND,
};
