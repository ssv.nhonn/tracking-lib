const SCREEN_NAME = {
  HOME: 'Home',
  PROFILE: 'Profile',
  EDIT_PROFILE: 'EditProfile',
  VOUCHER_DETAIL: 'VoucherDetail',
  PRIVACY_POLICY: 'PrivacyPolicy',
  TERMS_AND_CONDITIONS: 'TermsAndConditions',
  POINT_HISTORY: 'PointHistory',
  TRANSACTION_HISTORY: 'TransactionHistory',
  TRANSACTION_DETAIL: 'TransactionDetail',
  NEWS_DETAIL: 'NewsDetail',
  CAMPAIGN_DETAIL: 'CampaignDetail',
  STORE_LOCATIONS: 'StoreLocations',
  LANGUAGE: 'Language',
  ABOUT_US: 'AboutUs',
  LOGIN: 'Login',
  LOGIN_WITH_PHONE: 'LoginWithPhone',
  UPDATE_PHONE: 'UpdatePhone',
  STATION: 'Station',
  GAME: 'Game',
  COMPLETE_PROFILE: 'CompleteProfile',
  DELETE_ACCOUNT: 'DeleteAccount',
  FEEDBACK: 'Feedback',
  NOT_FOUND: 'NotFound',
  E_INVOICE: 'EInvoice',
  BANK_LIST: 'BankList',
  QR_BILL_SUPPORT: 'QrBillSupport',
  ABOUT_LOYALTY_TIER: 'AboutLoyaltyTier',
  LOYALTY_TIER: 'LoyaltyTier',
  DELIVERY: 'Delivery',
  NOTIFICATION_CENTER: 'notification_center',
  PAY: 'Pay',
  PROFILE_TAB: 'ProfileTab',
  SCAN_BARCODE: 'ScanBarcode',
  VOUCHER_LIST: 'VoucherList',
};

const MODAL_NAME = {
  OTP: 'OTP',
  CAPTCHA: 'Captcha',
  CONFIRM_PHONE_NUMBER: 'ConfirmPhoneNumber',
  LOGOUT: 'Logout',
  PREVIEW_VOUCHER_IMAGE: 'PreviewVoucherImage',
  PICK_IMAGE: 'PickImage',
  PICK_DATE: 'PickDate',
  ERROR_MESSAGE: 'ErrorMessage',
};

const BOTTOM_SHEET_NAME = {
  PAYMENT_METHODS_BOTTOM_SHEET: 'PaymentMethodsBottomSheet',
  APPLICABLE_PRODUCTS_BOTTOM_SHEET: 'ApplicableProductsBottomSheet',
  INVOICE_ISSUE_CONFIRMATION_BOTTOM_SHEET: 'InvoiceIssueConfirmationBottomSheet',
  GUIDE_QR_BILL_BOTTOM_SHEET: 'GuideQRBottomSheet',
};

export const SCREEN = {
  SCREEN_NAME,
  MODAL_NAME,
  BOTTOM_SHEET_NAME,
};
