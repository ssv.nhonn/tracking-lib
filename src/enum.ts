export enum Platform {
  ANDROID = 'ANDROID',
  IOS = 'iOS',
  WEB = 'web',
  UNKNOWN = 'unknown',
}

export enum FrontendSource {
  APP = 'Native App',
  MOMO = 'MoMo Mini App',
  WEB = 'Web Browser',
}
