import { FrontendSource, Platform } from './enum';

export interface UserInput {
  id: number;
  code: string;
  point: number;
}

export interface UserProperties {
  user_id: number;
  customer_id: number;
  user_code: string;
  point_balance: number;
}

export interface DeviceInfo {
  utm_source: string;
  platform: Platform;
  app: FrontendSource;
}

export interface TrackingInputInfo {
  name: string;
  screenName: string;
  properties: object;
}

export interface CommonProperties {
  hour_of_day: number;
  day_of_week: string;
  day_of_month: number;
  utm_source: string;
  platform: Platform;
  page_title: string;
  front_end: FrontendSource;
}

export interface CampaignProperies {
  campaign_id: number;
  campaign_name: string;
}

export interface CampaignCommonProperties {
  campaigns?: CampaignProperies[];
}

export interface ListingProperties {
  item_id: number;
  item_name: string;
  price: number;
  quantity: number;
  index: number;
  item_category?: string;
  item_category2?: string;
  item_category3?: string;
}

export interface ListingCommonProperties {
  items?: ListingProperties[];
}

export interface PromotionProperties {
  promotion_id: number;
  promotion_name: string;
}

export interface PromotionCommonProperties {
  promotions: PromotionProperties[];
}

export interface VoucherProperies {
  item_id: number;
  item_name: string;
}

export interface VoucherCommonProperties {
  vouchers?: VoucherProperies[];
}

export interface StoreProperties {
  store_id: number;
  store_name: string;
}

export interface StoreCommonProperties {
  stores?: StoreProperties[];
}

export interface TransactionProperties {
  order_id: number;
  transaction_id: number;
}

export interface TrackingProperties {
  [key: string]: number | string;
}

export interface TrackingData {
  user_properties: UserProperties | undefined;
  events: { name: string; params: TrackingProperties }[];
}

export interface SendOptions {
  previewToken?: string;
}

export type TrackerType = 'REST_API' | 'FIREBASE' | 'GTAG';
