import { COMMON } from './constants';
import {
  CommonProperties,
  DeviceInfo,
  TrackerType,
  TrackingData,
  TrackingInputInfo,
  TrackingProperties,
  UserInput,
  UserProperties,
} from './interface';

export default class Tracker {
  private url: string;
  private previewToken: string | null = null;
  private user: UserProperties | null = null;
  private device: DeviceInfo | null = null;
  private isInitialized: boolean = false;
  private trackerType: TrackerType;

  /**
   * Constructs a new instance of the API class.
   * @param {string} url - The URL for the API.
   * @param {string} [previewToken] - The preview token (optional).
   */
  constructor(url: string, trackerType: TrackerType, previewToken?: string) {
    this.url = url;
    this.trackerType = trackerType;
    previewToken && (this.previewToken = previewToken);
  }

  /**
   * Sets the user information.
   *
   * @param {UserInput} user - The user information to be set.
   * @returns {void} - Returns nothing.
   */
  setUserProperties(user: UserInput): void {
    const { id, code, point } = user;

    this.user = {
      user_id: id,
      customer_id: id,
      user_code: code,
      point_balance: point,
    };
  }

  /**
   * Sets the point balance for the user.
   *
   *  @param balance - The new point balance for the user.
   */
  setPointBalance(balance: number): void {
    this.user.point_balance = balance;
  }

  /**
   * Set the device information.
   *
   * @param {DeviceInfo} device - The device information to set.
   * @returns {void}
   */
  setDeviceInfo(device: DeviceInfo): void {
    this.device = { ...device };
  }

  /**
   * Initializes the user and device information.
   *
   * @param {DeviceInfo} device - The device information object.
   * @return {void} No return value.
   */
  initialize(device: DeviceInfo): void {
    this.setDeviceInfo(device);
    this.isInitialized = true;
  }

  /**
   * Destroys the object by setting the `user` and `device` properties to null and the `isInitialized` property to false.
   *
   * @param {void} - This function does not accept any parameters.
   * @return {void} - This function does not return any value.
   */
  destroy(): void {
    this.user = null;
    this.device = null;
    this.isInitialized = false;
  }

  transform(event: TrackingInputInfo): TrackingData {
    if (!this.isInitialized) return;

    const { name, properties, screenName } = event;
    const { utm_source, platform, app } = this.device;
    const today = new Date();

    const commonProperties: CommonProperties = {
      hour_of_day: today.getHours(),
      day_of_week: COMMON.DAY_OF_WEEK[today.getDay()],
      day_of_month: today.getDate(),
      utm_source,
      platform,
      page_title: screenName,
      front_end: app,
    };

    const trackingProperties: TrackingProperties = { ...properties, ...commonProperties };

    const data: TrackingData = {
      user_properties: this.user ? this.user : undefined,
      events: [{ name, params: trackingProperties }],
    };

    return data;
  }

  track(event: TrackingInputInfo): void {
    if (!this.isInitialized) return;

    const data = this.transform(event);

    const headers = {
      'Content-Type': 'application/json',
      'X-Gtm-Server-Preview': this.previewToken,
    };

    if (!this.previewToken) {
      delete headers['X-Gtm-Server-Preview'];
    }

    fetch(this.url, {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      headers,
      body: JSON.stringify(data),
    });
  }
}
